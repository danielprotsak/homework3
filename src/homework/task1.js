import React, {useState, useRef, useContext} from "react";
import Toggler , {TogglerItem} from "../classwork/task1";
import MyInput from "../classwork/task2";

const HomeWorkForm = () => {

    let[inputValue,setInputValue] = useState("");
    let[file,setFile] = useState('');
    let[imgSrc,setImgSrc] = useState("https://cdn.onlinewebfonts.com/svg/img_212908.png");
    const togglerRef = useRef(null);
    const onFormSubmit = (e) => {
        e.preventDefault();
        const obj = {
            name:inputValue,
            file:file,
            toggler:togglerRef.current.state.value
        }
        console.log(obj);
    }
    const onInputChange = (e) => {
        const value = e.target.value;
        setInputValue(value);
    }
    const onChangeFile = (e) => {
        console.log(e.target.files[0]);
        const file = e.target.files[0];
        let reader = new FileReader();
        reader.onloadend = () => {
            setImgSrc(reader.result);
            setFile(file);
        }
        reader.readAsDataURL(file);

    }

    const placeholderOnClick = (e) => {
        const test = document.querySelector("#test");
        test.click();
    }
    return(
        <form onSubmit={onFormSubmit}>
            <div style={{display:"inline-grid"}}>
                <MyInput type="text"
                         placeholder="placeholder"
                         value={inputValue}
                         onChange={onInputChange}
                         contentLength
                         contentMaxLength={4}
                />
                <Toggler ref={togglerRef}>
                    <TogglerItem toggleKey="qwerty" value="Android"/>
                    <TogglerItem toggleKey="asdsa" value="Apple"/>
                </Toggler>
                <img onClick={placeholderOnClick} src={imgSrc} width="150" height="150" alt="dsads"/>
                <input id="test" type="file" onChange={onChangeFile} style={{display:'none'}}/>
                <input type="submit"/>
            </div>

        </form>
    );
}
export default HomeWorkForm;