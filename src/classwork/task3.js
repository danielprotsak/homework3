import React,{useEffect,useState,useRef} from "react";
import MyInput from "./task2";
import PropTypes from 'prop-types'
import Toggler,{TogglerItem} from "./task1";
const Form = () => {
    let[name,setName] = useState("");
    let[password,setPassword] = useState("");
    let[language,setLanguage] = useState("");
    const togglerRef = useRef(null);
    const onFormSubmit = (e) => {
        e.preventDefault();
        const data = {
            name:name,
            password:password,
            gender:togglerRef.current.state.value,
            language:language,
        }
        console.log(data);

    }
    const onChangeName = (e) => {
        const value = e.target.value;
        setName(value);
    }
    const onChangePassword = (e) => {
        const value = e.target.value;
        setPassword(value);
    }
    const onChangeLanguage = (e) => {
        const value = e.target.value;
        setLanguage(value);
    }
    return(
        <form onSubmit={onFormSubmit}>
            <div style={{display:"inline-grid"}}>
                <label htmlFor="name">Enter name</label>
                <input type="text" name="name" value={name} onChange={onChangeName}/>
                <label htmlFor="password">Enter password</label>
                <input type="password" name="password" value={password} onChange={onChangePassword}/>
                <span>Gender</span>
                <Toggler ref={togglerRef}>
                       <TogglerItem toggleKey="qwerty" value="Men"/>
                       <TogglerItem toggleKey="asdsa" value="Female"/>
                </Toggler>
                <label htmlFor="language">Enter language</label>
                <input type="text" name="language" value={language} onChange={onChangeLanguage}/>
                <input type="submit"/>
            </div>

        </form>
    );
}
export default  Form;
