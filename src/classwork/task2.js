import React,{Component,Fragment} from "react";
import PropTypes from 'prop-types'
class MyInput extends Component{
    state = {textValue:""}
    constructor(props) {
        super(props);
    }

    render = () => {
        const{type,placeholder,value,name,contentLength,contentMaxLength,onChange} = this.props;
        let input =  <input type={type} name={name} onChange={onChange} value={value} placeholder={placeholder}/>
        if(contentMaxLength !== undefined){
            if(value.length < contentMaxLength){
                input = React.cloneElement(input,{...input.props});
            }else{
                const fixedSizeValue = value.substring(0,contentMaxLength);
                input = React.cloneElement(input,{...input.props,value:fixedSizeValue});
            }
        }
        const validatedValue = input.props.value;
        return(
            <Fragment>
                {input}
                {contentLength === true &&
                    <span>Symbol count {validatedValue.length}</span>
                }
            </Fragment>
        );
    }
}
MyInput.propTypes = {
    type:PropTypes.oneOf(['text','password','number']).isRequired,
    placeholder:PropTypes.string,
    value:PropTypes.string.isRequired,
    onChange:PropTypes.func.isRequired,
    contentLength:PropTypes.bool,
    contentMaxLength: PropTypes.number
}
export default MyInput;